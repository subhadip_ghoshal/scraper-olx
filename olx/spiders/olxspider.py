import scrapy
from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule

from olx.items import OlxItem


class OlxSpider(CrawlSpider):
    name = 'olxspider'
    allowed_domains = ['olx.pt']
    start_urls = ['http://www.olx.pt']

    rules = (
        Rule(LinkExtractor(allow=r'', restrict_xpaths='//div[@id="Other-Cats"]')),
        Rule(LinkExtractor(allow=r'', restrict_xpaths='//div[@id="div_pagination"]'), callback='parse_item', follow=True)
    )

    def parse_item(self, response):
        item = OlxItem()

        results = response.xpath('//div[@id="resultlist"]/div[@id="page"]')

        for result in results:
            item['title'] = result.xpath('div[@class="ti"]/a/text()').extract()
            item['price'] = result.xpath('div[@class="ti"]/div[@class="price"]/text()').extract()
            item['time'] = result.xpath('div[@class="time"]/i/text()').extract()[0].strip()
            item['catloc'] = result.xpath('div[@class="catloc"]/a/text()').extract()
            item['catloc_link'] = result.xpath('div[@class="catloc"]/a/@href').extract()
            item['adlink'] = result.xpath('div[@class="ti"]/a/@href').extract()
            yield item

