# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class OlxItem(scrapy.Item):
    title = scrapy.Field()
    price = scrapy.Field()
    time = scrapy.Field()
    catloc = scrapy.Field()
    catloc_link = scrapy.Field()
    adlink = scrapy.Field()
